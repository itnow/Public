function Start-Archive {
    <#
        .SYNOPSIS
            Script is used to archive files

        .DESCRIPTION
            This script will let you copy or move files to a different location based off of last write/access times.
            There is also an option to create symbolic links for the files that are moved.
            There is also an option to set a 'StopTime' time frame that will stop execution durning that time.
            Script has the ability to resume where it left off when caching is enabled.

        .PARAMETER Source
            The path to the directory you want archived.
            Example: 'C:\Windows\Temp'

        .PARAMETER Destination
            The path to the directory you want data archived to.
            Example: 'D:\Archive'

        .PARAMETER Days
            How old, in days, do files need to be before they are archived
            Example: 365
            To archive items older then one year.

        .PARAMETER StopTimeStart
            Used to create an exclusion time frame. The script will not run between these hours. The start hour of the exclusion time frame.
            Example: 11
            Will set the start of the exclusion time frame for 11:00AM

        .PARAMETER StopTimeEnd
            Used to create an exclusion time frame. The script will not run between these hours. The end hour of the exclusion time frame.
            Example: 18
            Will set the end of the exclusion time frame for 6:00PM

        .PARAMETER Copy
            Used to copy archive files instead of moving them.

        .PARAMETER MakeSymLinks
            Used to create symbolic links to files moved durning archive process.

        .PARAMETER EnableCaching
            Used to enable resuming an archive process stopped by StopTime.

        .PARAMETER ShowReport
            Will launch the report on completion.

        .OUTPUTS
            Log file stored in $Source\ArchiveReport.html

        .NOTES
            Version:        1.0
            Author:         Chris Taylor
            Creation Date:  5/15/2019
            Purpose/Change: Initial script development

        .LINK
          christaylor.codes
  
        .EXAMPLE
            Start-Archive -Source = 'C:\Temp' -Destination = 'D:\Temp' -Days = 1 -StopTimeStart = 7 -StopTimeEnd = 18 -Copy = $true -EnableCaching = $true -ShowReport = $true -Verbose = $true
            This will create an archive job that will copy files from C:\Temp to D:\Temp that are older then one day. The script will stop execution between the hours of 7:00AM and 6:00PM.

        .EXAMPLE
            Start-Archive -Source = 'C:\Temp' -Destination = 'D:\Temp' -Days = 1 -EnableCaching = $true -MakeSymLinks = $true -ShowReport = $true -Verbose = $true
            This will create an archive job that will move files from C:\Temp to D:\Temp that are older then one day. It will also create symbolic links in the source directory to the archive location.
    #>
    [CmdletBinding()]
    Param (
        [Parameter(Position=0,mandatory=$true)]
        [ValidateScript({Test-Path $_ -PathType 'Container'})]
        [string]$Source,
        [Parameter(Position=1,mandatory=$true)]
        [ValidateScript({Test-Path $_ -PathType 'Container'})]
        [string]$Destination,
        [Parameter(Position=2,mandatory=$true, ParameterSetName='Default')]
        [int]$Days,
        [int]$StopTimeStart,
        [int]$StopTimeEnd,
        [Parameter(ParameterSetName='Copy')]
        [switch]$Copy,
        [Parameter(ParameterSetName='Move')]
        [switch]$MakeSymLinks,
        [switch]$EnableCaching,
        [switch]$ShowReport
    )
 
    #region-[Initializations]--------------------------------------------------------
        #Script Version
        $ScriptVersion = "1.0"

        #Requires -Version 5
        if ($Host.Version.Major -lt 5 -and $MakeSymLinks) {
            Write-Error "Powershell Version 5+ is required for symbolic links."
            exit 1
        }
    
        # Test Destination
        if ((Test-Path $Destination) -eq $false) {
            Write-Output "Unable to access destination, $Destination"
            exit 1
        }

        # Make sure days is a negative number
        if ($Days -gt 0) {
            $Days = $Days * -1
        }

        # Errors durning archive
        $ArchiveErrors = @()

        # Used for stop time
        $InTime = $true

        # Initialize cache file name
        $CacheFileName = $Source
        [System.IO.Path]::GetInvalidFileNameChars() | ForEach-Object {$CacheFileName = $CacheFileName.replace($_,'.')}

    #endregion Initialisations 
 
    #region-[Functions]------------------------------------------------------------

        function New-ArchiveReport {
            param(
                $Summary,
                $ArchiveErrors,
                $ReportName = 'ArchiveReport.html'
            )
            $ReportPath = $(Join-Path $Summary.Source $ReportName)
            $Head = @"
This is a report generated by the automatic archive process.
$($Summary | ConvertTo-Html -Fragment)
"@
            if ($ArchiveErrors.count -gt 0) {
                $Head += '<h2>Errors</h2>'
            }
            $Table = $ArchiveErrors | ConvertTo-Html -Fragment
            $Template = @'
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>Automate Agent Signup</title>
  </head>
  <body>
    <div class="row">
		<div class="col">
		</div>
		<div class="col-md-auto">
			<h1>Archive Report</h1>
            @HEAD@
            @TABLE@
        </div>
	    <div class="col">
	    </div>
	</div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>
'@
    
            # Write to file
            $Template -replace ('@HEAD@', ($Head).replace('<table>','<table class="table table-dark table-striped table-hover">')) -replace ('@TABLE@', ($Table).replace('<table>','<table class="table table-dark table-striped table-hover">')) | Out-File $ReportPath -Force
            return $ReportPath
        }

        function Save-Cache {
            param(
                $CacheArray,
                $Path
            )
            $CacheArray | Out-File $Path -Force
            $null = Remove-Item "$($Path).zip" -Force -ErrorAction SilentlyContinue
            $FileName = Split-Path $Path -Leaf
            Set-Content "$($Path).zip" ("PK" + [char]5 + [char]6 + ("$([char]0)" * 18))
            (dir "$($Path).zip").IsReadOnly = $false
            Start-Sleep -Milliseconds 5
            $shellApplication = new-object -com shell.application
            $zipPackage = $shellApplication.NameSpace("$($Path).zip")
            $zipPackage.CopyHere($Path)
            $size = $zipPackage.Items().Item($FileName).Size
            while($zipPackage.Items().Item($FileName) -Eq $null) {start-sleep -seconds 1}
            Remove-Item $Path
        }

        function Load-Cache {
            param(
                $Path    
            )
            Add-Type -assembly "system.io.compression.filesystem"
            [io.compression.zipfile]::ExtractToDirectory("$($Path).zip", $(Split-Path $Path -Parent))
            $Result = Get-Content $Path
            Remove-Item $Path
            return $Result
        }

    #endregion Functions
 
    #region-[Execution]------------------------------------------------------------
 
        # Caching
        if ($EnableCaching -and (Test-Path "$($Destination)\$($CacheFileName).zip")) {
            # Load from cache
            $LoadCache = {
                Write-Verbose "Cached archive list found."
                $FilesToArchive = Load-Cache "$($Destination)\$($CacheFileName)"
        
                # Check to see if we have already started
                if (Test-Path "$($Destination)\$($CacheFileName).count") {
                    [int]$CachedPosition = Get-Content "$($Destination)\$($CacheFileName).count"
                    if ($CachedPosition -lt $FilesToArchive.Count) {
                        Write-Verbose "Resuming from cached position $($CachedPosition)/$($FilesToArchive.Count)"
                    }
                    else {
                        $CachedPosition = 0
                    }
                } else {
                    $CachedPosition = 0
                }
            }
            $CacheLoadTime = Measure-Command $LoadCache
            Write-Verbose "Cache loaded: $($CacheLoadTime.ToString())"

        } 
        # New lookup
        else {
            # Gather a list of all files in $Source
            Write-Verbose "Starting enumeration of $Source"
            $Enumerate = {
                $SourceFiles = Get-ChildItem $Source -Filter * -Recurse -ErrorAction SilentlyContinue
            }
            $EnumerateTime = Measure-Command $Enumerate
            $TotalCount = $SourceFiles.Count
            Write-Verbose "$($TotalCount) files found in source, $Source : $($EnumerateTime.ToString())"
    
            # Filter down to files that need archiving
            $Filter = {
                $FilesToArchive = $SourceFiles | Where-Object {$_.LastAccessTime -lt (Get-Date).AddDays($Days) -and $_.LastWriteTime -lt (Get-Date).AddDays($Days) -and $_.PSIsContainer -eq $false -and $_.Attributes -notcontains 'ReparsePoint'}
                $FilesToArchive = $FilesToArchive | Select-Object -ExpandProperty FullName 
            }
            $FilterTime = Measure-Command $Filter
            Write-Verbose "Filtered to $($FilesToArchive.Count) files to archive: $($FilterTime.ToString())"
            
            # Clear out large variable
            Remove-Variable SourceFiles

            if ($EnableCaching) {
                # Cache directory enumeration
                $CacheEnumerated = {
                    Save-Cache -CacheArray $FilesToArchive -Path "$($Destination)\$($CacheFileName)"
                }
                $CacheEnumeratedTime = Measure-Command $CacheEnumerated
                Write-Verbose "Cached in $($CacheEnumeratedTime.ToString())"
            }
            $CachedPosition = 0
        }

        # Process each file
        for ($I = $CachedPosition;$I -le $FilesToArchive.Count -1 -and $InTime) {
        
            # Get file path from list
            $File = $FilesToArchive[$I]

            Write-Progress -Activity Archiving -Status "$I/$($FilesToArchive.Count)" -PercentComplete ($I / $FilesToArchive.count * 100) -CurrentOperation $File
            if ($StopTimeStart -and $StopTimeEnd -and ((Get-Date).TimeOfDay.Hours -lt $StopTimeStart -or (Get-Date).TimeOfDay.Hours -ge $StopTimeEnd) -eq $false) {
                Write-Warning 'Out of time!'
                $InTime = $false
                continue
            }

            try {
                # Format new paths
                $FileDir = Split-Path $File -Parent
                $NewDir = $($FileDir.replace($Source, $Destination))
                $Target = $(Join-Path $NewDir $(Split-Path $File -Leaf))

                # Check if directory has been created
                if ((Test-Path $NewDir) -eq $false) {
                    $Null = Copy-Item $FileDir $NewDir -Force
                }
        
                if ($Copy) {
                    # Copy the file to the new location
                    Copy-Item $File -Destination $NewDir -Force -ErrorAction Stop
                } 
                else {
                    # Move the file to the new location
                    Move-Item $File -Destination $NewDir -Force -ErrorAction Stop
        
                    if ($MakeSymLinks) {
                        # Create symbolic link
                        $null = New-Item -ItemType SymbolicLink -Path $FileDir -Name $(Split-Path $File -Leaf) -Value $Target -ErrorAction Stop
                    }                
                }
            }
            catch {
                Write-Warning "$($_.Exception.Message) $File"
                $ArchiveErrors += [pscustomobject]@{
                    File = $File
                    Error = $_.Exception.Message
                }
            }        
            $I++
            if ($EnableCaching) {
                $I | Out-File "$($Destination)\$($CacheFileName).count" -Force
            }        
        }
    
        # Clean up cache
        if ($InTime) {
            Remove-Item "$($Destination)\$($CacheFileName).count" -Force -ErrorAction SilentlyContinue
            Remove-Item "$($Destination)\$($CacheFileName).zip" -Force -ErrorAction SilentlyContinue
        }
        else {
            Write-Verbose "Cache saved because time limit was hit."
        }
    
        # Report
        $Summary = [pscustomobject]@{
            Source = $Source
            Destination = $Destination
            'Files to Archive' = $FilesToArchive.Count
            'Files Total' = $TotalCount
            Runtime = $(Get-Date)
            Errors = $ArchiveErrors.Count
            Cache = [boolean]$EnableCaching.IsPresent
            Position = $I
            'Finish in time' = $InTime
        }
        if ($EnumerateTime) {
            Add-Member -InputObject $Summary -Name 'Time to enumerate' -Value $EnumerateTime.ToString() -MemberType NoteProperty
        }
        if ($CacheLoadTime) {
            Add-Member -InputObject $Summary -Name 'Cache load time' -Value $CacheLoadTime.ToString() -MemberType NoteProperty
        }

        $Report = New-ArchiveReport -Summary $Summary -ArchiveErrors $ArchiveErrors
        if ($ShowReport) {
            .$Report
        }
    
    #endregion Execution
}

<#
$ArchiveParams = @{
    Source = 'C:\Temp'
    Destination = 'D:\Temp'
    Days = 1
    EnableCaching = $true
    MakeSymLinks = $true
    ShowReport = $true
    Verbose = $true
}
Start-Archive @ArchiveParams
#>
