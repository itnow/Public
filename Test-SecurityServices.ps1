# This script is used to test a computers security
# Content Filtering, Gateway AV, Resident AV, SSL inspection

$Categories = [pscustomobject]@{
    Alchohol = 'wine-searcher.com', 'beeradvocate.com', 'merchantduvin.com', 'drizly.com'
    Dating = 'badoo.com', 'match.com', 'pof.com', 'okcupid.com'
    Gambling = 'bet365.com', 'freelotto.com', 'williamhill.com', 'betfair.com'
    Lingerie = 'victoriassecret.com', 'wonderbra.com', 'figleaves.com', 'lasenza.co.uk'
    Nudity = 'modelmayhem.com', 'egotastic.com', 'fineartnude.com', 'wwtdd.com'
    Pornography = 'xvideos.com', 'xhamster.com', 'pornhub.com', 'xnxx.com'
    Weapons = 'cabelas.com', 'budsgunshop.com', 'gunbroker.com', 'ar15.com'
    P2P = 'imesh.com', 'emule.com', 'torrent2exe.com', 'truxshare.com'
    Hacking = 'hackforums.net', 'elhacker.net', 'spyarsenal.com', 'agenttesla.com'
}

function Test-SSLDPI {
    $request = [System.Net.HttpWebRequest]::Create('https://google.com')
    $request.GetResponse().Dispose()
    $servicePoint = $request.ServicePoint
    $servicePoint.Certificate.Issuer -notmatch 'google'
}

foreach ($Category in $Categories.psobject.Properties.Name) {
    Write-Output "Testing $Category"
    $Sites = $Categories.$Category
    foreach ($Site in $Sites) {
        try {
            $test = iwr $Site
            Write-Host "  $Site Allowed" -ForegroundColor Red
        }
        catch {
            Write-Host "  $Site Blocked" -ForegroundColor Green
        }
    }    
}
Write-Output '----'

# Test sample virus download
Write-Output "Testing virus download and execution"
iwr 'http://www.eicar.org/download/eicar.com' -OutFile "$env:TEMP\eicar.com"
if ((Get-Content "$env:TEMP\eicar.com") -match 'EICAR-STANDARD-ANTIVIRUS-TEST-FILE') {
    Write-Host " Test Virus was downloaded" -ForegroundColor Red
    try {
        & "$env:TEMP\eicar.com"
    } 
    catch {
        Write-Host " File was removed on execution." -ForegroundColor Green
    }
} else {
    Write-Output " File download was blocked"
}
Write-Output '----'

Write-Output "Testing for SSL DPI"
$SSLDPI = Test-SSLDPI
$Color = 'Green'
if ($SSLDPI) {
    Write-Host " SSL inspection enabled" -ForegroundColor Green
}
else {
    Write-Host " SSL inspection disabled" -ForegroundColor Red
}
