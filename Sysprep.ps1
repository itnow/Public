# This script is used to Sysprep a machine for use as a new computer.
If (-not ([bool](([System.Security.Principal.WindowsIdentity]::GetCurrent()|Select-object -Expand groups -EA 0) -match 'S-1-5-32-544'))) {
    Throw "Needs to be ran as Administrator" 
}

$choice = ""
while ($choice -notmatch "[y|n]"){
    $choice = read-host "Are you sure you want to Sysprep this machine? [Y/N]"
}

if ($choice -eq "y"){
    Write-Output 'Gathering application list'
    $Applications = Get-CimInstance -Class Win32_Product
    
    $Veeam = $Applications | where {$_.name -eq 'Veeam Agent for Microsoft Windows'}
    if ($Veeam) {
        Write-Output "Uninstalling Veeam Agent"
        $null = Stop-Service VeeamEndpointBackupSvc -Force
        $null = Get-Process Veeam.EndPoint.* | Stop-Process -Force
        $null = $Veeam | Invoke-CimMethod -MethodName Uninstall
    }
    
    $ScreenConnect = $Applications | where {$_.name -eq 'ScreenConnect Client (2fed754e7d45a198)'}
    if ($ScreenConnect) {
        Write-Output "Uninstalling ScreenConnect"
        
        $Apps = Get-ChildItem 'HKLM:\SOFTWARE\WOW6432Node\Microsoft\Windows\CurrentVersion\Uninstall'
        $ScreenConnect = $Apps | where {$_.GetValue('DisplayName') -like "ScreenConnect Client (*)"}
        foreach($Instance in $ScreenConnect){
            $ArgumentList = @($Instance.GetValue('UninstallString').replace('MsiExec.exe ',''), '/qn')
            $null = Start-Process msiexec -ArgumentList $ArgumentList -Wait
        }
    }
    
    $Webroot = $Applications | where {$_.name -eq 'Webroot SecureAnywhere'}
    if ($Webroot) {
        Write-Output "Uninstalling Webroot"
        $null = $Webroot | Invoke-CimMethod -MethodName Uninstall
    }    

    Write-Output "Reinstalling Automate"
    irm 'https://bit.ly/ltposh' | iex
    Redo-LTService
    
    $defaultValue = 'n'
    $prompt = Read-Host "Do you want to skip removal of metro apps? Only skip if you are having issues. [Y/N]"
    $prompt = ($defaultValue,$prompt)[[bool]$prompt]
    if ($prompt -eq 'n') {
        Write-Output "Removing metro apps"
        $null = Get-AppxPackage -allusers | Remove-AppxPackage
        $null = Get-AppxProvisionedPackage -Online | Remove-AppxProvisionedPackage -allusers -Online
    }

    Write-Output "Starting Sysprep"
    Start-Process "C:\Windows\System32\Sysprep\sysprep.exe" -ArgumentList '/generalize /oobe /reboot'
}
    
else {
    Write-Output "Done!"
}