$RDGatewayAddress = Read-Host 'What is the gateway URL? (remote.domain.com)'
$Computer = Read-host 'What machine are they connecting to? (computer.domain.local)'
$User = Read-Host 'What is their username? (DOMAIN\ctaylor)'
$Default = @"
screen mode id:i:2
use multimon:i:1
session bpp:i:32
compression:i:1
keyboardhook:i:0
audiocapturemode:i:1
videoplaybackmode:i:1
connection type:i:7
networkautodetect:i:1
bandwidthautodetect:i:1
displayconnectionbar:i:1
enableworkspacereconnect:i:0
disable wallpaper:i:0
allow font smoothing:i:0
allow desktop composition:i:0
disable full window drag:i:1
disable menu anims:i:1
disable themes:i:0
disable cursor setting:i:0
bitmapcachepersistenable:i:1
audiomode:i:2
redirectprinters:i:1
redirectcomports:i:0
redirectsmartcards:i:1
redirectclipboard:i:1
redirectposdevices:i:0
autoreconnection enabled:i:1
authentication level:i:0
prompt for credentials:i:0
negotiate security layer:i:1
remoteapplicationmode:i:0
alternate shell:s:
shell working directory:s:
gatewayusagemethod:i:1
gatewaycredentialssource:i:4
gatewayprofileusagemethod:i:1
promptcredentialonce:i:1
gatewaybrokeringtype:i:0
use redirection server name:i:0
rdgiskdcproxy:i:0
kdcproxyname:s:
drivestoredirect:s:
gatewayhostname:s:$RDGatewayAddress
full address:s:$Computer
username:s:$User
"@
$DesktopPath = [Environment]::GetFolderPath("Desktop")
$RDPFilePath = "$DesktopPath\$($Computer).rdp"
$Default | Out-File $RDPFilePath -Force
Write-Output "File created, $RDPFilePath"